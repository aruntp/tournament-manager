# Tournament

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

# Usage Guide

This application is used for football tournament management, User can create brackets, save, update ,view seasons etc. 
The application uses Realtime Database from Firebase as backend

```
## Countries
    o List of countries and their ranks
    o Clicking on country will show the list of players of the team
    o Update the country data - TODO
    o Add/Delete the country data - TODO

## Players 
    o Shows the players for a country participating in the tournament
    o Players for a specific country can be viewed by selecting the dropdown
    o Click on a player will show the player details - TO DO
    o Update the player data - TODO
    o Add/Delete a player from the squad - TODO

## Bracket
    o Shows the tournament bracket
    o Results can be updated from here
    o After updating one round, the next round bracket is displayed
    o Clicking on one card, will display the details of match
    o View the complete details of match, update - TO DO
    o Save the season using the 'Save Season' button
    o Reload and use the stored data - TO DO
    o Enable back navigation and forward navigation - persist data - TO DO
    o Show the user input form on the details screen - TO DO
    o Beautify the view - TO DO


## Management
    o View different brackets for seasons - TO DO
    o Select past seasons - TO DO
    o Manage the tournament data - TO DO

## Management
    This is an incomplete project, lot of functionalities still needs to be added

```
# Installation

Download this project, The angular version 13 requires NodeJS version 14.15.0 or 16.10.0. Download and install it from `https://nodejs.org/en/download/`
Go to the folder 'voting-app' and run `npm install`

## Deployment URL

The application is deployed and can be accessed through - `https://tournament-view.web.app` . 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Unit tests are minimal, need to update and have a code coverage of atleast 80%

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
