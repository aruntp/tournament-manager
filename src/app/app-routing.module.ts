import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryresolverService } from './common-services/countryresolver.service';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'players',
    loadChildren: () => import('./player/player.module').then(m => m.PlayerModule),
  },
  {
    path: 'brackets',
    loadChildren: () => import('./bracket/bracket.module').then(m => m.BracketModule),
  },
  {
    path: 'management',
    loadChildren: () => import('./management/management.module').then(m => m.ManagementModule),
  },
  {
    path: '',
    /*resolve: {
      routeResolver: CountryresolverService
    }, */
    redirectTo: '/countries', pathMatch: 'full'
  },
  { 
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [CountryresolverService]
})
export class AppRoutingModule { }
