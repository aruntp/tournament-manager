export interface Player {
    age: number;
    id: number;
    name: string;
    photo: string;
    position: string;
}