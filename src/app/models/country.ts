export interface Country {
    code: string;
    rank: number;
    flag: string;
    name: string;
  }