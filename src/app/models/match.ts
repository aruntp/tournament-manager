import { Country } from "./country";

export interface Match {
    homeTeam: Country;
    awayTeam: Country;
    matchScore: string;
}