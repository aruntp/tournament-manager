import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerManagementComponent } from './player-management/player-management.component';
import { BracketManagementComponent } from './bracket-management/bracket-management.component';



@NgModule({
  declarations: [
    PlayerManagementComponent,
    BracketManagementComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ManagementModule { }
