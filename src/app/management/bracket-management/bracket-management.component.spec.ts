import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BracketManagementComponent } from './bracket-management.component';

describe('BracketManagementComponent', () => {
  let component: BracketManagementComponent;
  let fixture: ComponentFixture<BracketManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BracketManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BracketManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
