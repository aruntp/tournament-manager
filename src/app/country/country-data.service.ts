import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryDataService {

  /**
   * Back end HTTP calls are declared here which are used by the country module
   * Not everything are used now, but saved for future
   */
  apiUrl = "https://tournament-server-default-rtdb.europe-west1.firebasedatabase.app/";
  
  constructor(private http: HttpClient) { }

  createCountry(country: Country) {
    return this.http.post(this.apiUrl, country);
  }

  editCountry(country: Country) {
    return this.http.put(this.apiUrl, country);
  }

  deleteCountry() {
    return this.http.delete(this.apiUrl);
  }

  getCountries() {
    return this.http.get(this.apiUrl + 'countries.json');
  }

  getCountryDetails(name: string) {
    return this.http.get(this.apiUrl + name + '.json');
  }

}