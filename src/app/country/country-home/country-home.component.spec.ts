import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { CountryHomeComponent } from './country-home.component';

describe('CountryHomeComponent', () => {
  let component: CountryHomeComponent;
  let fixture: ComponentFixture<CountryHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryHomeComponent ],
      providers: [HttpClient, HttpHandler, 
        {provide: ActivatedRoute, useValue: {
          params: of({id: 'Belgium'})
        }}, Router]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
