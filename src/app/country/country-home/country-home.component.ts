import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { CountryDataService } from '../country-data.service';
import { Country } from 'src/app/models/country';
import { TournamentService } from 'src/app/common-services/tournament.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-country-home',
  templateUrl: './country-home.component.html',
  styleUrls: ['./country-home.component.scss']
})
export class CountryHomeComponent implements OnInit {

  countriesAvailable : Country[] = [];
 
  constructor(private countryService: CountryDataService, private tournamentService: TournamentService, private route: ActivatedRoute, private router: Router) { 
  }

  /**
   * Get the countries from DB for the tournament, which are qualified for the group stage
   */
  ngOnInit(): void {
     this.countryService.getCountries().pipe(first()).subscribe(
       (cnt: Object) => {
         Object.assign(this.countriesAvailable, cnt);
         this.tournamentService.selectedTeams = this.countriesAvailable;
       }
     )
  }

  // generateBrackets() {
  //   this.tournamentService.selectedTeams = this.countriesAvailable;
  // }

  /**
   * navigate to see players for a country
   * TODO - Go to the country view and display the details, and from there show the players list
   * @param country country name
   */
  viewCountry(country: Country) {
    this.router.navigate(['/players', country.name])
  }

}