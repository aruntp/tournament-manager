import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { CountryDataService } from './country-data.service';

describe('CountryDataService', () => {
  let service: CountryDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpClient, HttpHandler]
    });
    service = TestBed.inject(CountryDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
