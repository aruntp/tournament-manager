import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCountryComponent } from './add-country/add-country.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountryHomeComponent } from './country-home/country-home.component';

const countryRoutes: Routes = [
    { path: 'countries', component: CountryHomeComponent },
    { path: 'countries/country', component: AddCountryComponent },
    { path: 'countries/country/:id', component: CountryDetailsComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(countryRoutes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
