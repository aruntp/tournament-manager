import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryDataService } from '../country-data.service';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit {

  teamData:any = [];
  constructor(private route: ActivatedRoute, private countrySrv: CountryDataService) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.params);
    const countryName = this.route.snapshot.params['id'];
    this.countrySrv.getCountryDetails(countryName).subscribe(
      data => {
        this.teamData = data;
      }
    )
  }

  viewPlayer(player: string) {

  }

}
