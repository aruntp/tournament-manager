import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryHomeComponent } from './country-home/country-home.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { CountryRoutingModule } from './country-routing.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    CountryHomeComponent,
    AddCountryComponent,
    CountryDetailsComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    CountryRoutingModule,
    DragDropModule
  ]
})
export class CountryModule { }
