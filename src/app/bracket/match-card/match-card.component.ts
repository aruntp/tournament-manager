import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { MatchSummary } from '../models/matchsummary';
import { BracketHttpService } from '../services/bracket-http.service';
import { BracketService } from '../services/bracket.service';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss']
})
export class MatchCardComponent implements OnInit {

  @Input() match: any;
  @Input() round: number = 0;
  homeScore: number = 0;
  awayScore: number = 0
  matchSummary: MatchSummary = new MatchSummary();

  constructor(private bracketService: BracketService,
     private datePipe: DatePipe,
     private http: BracketHttpService) { }

  ngOnInit(): void {

  }

  /**
   * Modify the date to the format for saving
   * @param event date input from datepicker
   */
     saveDate(event: any) {
      this.matchSummary.date = this.datePipe.transform(event.value, 'yyyy-MM-dd');
    }

  /**
   * Update the match result, after validating the fields
   * TODO - In case of equality, penalities should be calculated 
   */
   updateMatch() {
    if (this.matchSummary.id === '') {
      this.http.postMatch(this.matchSummary).pipe(first()).subscribe(
        (status: any) => {
          this.matchSummary.id = status.name;
          alert("Result updated succesfully");
        }
      )
    } else {
      this.http.updateMatch(this.matchSummary).pipe(first()).subscribe(
        (status: any) => {
          console.log(status);
          alert("Result updated succesfully")
        }
      )
    }
  }

  /**
   * Update the scores entered from UI
   */
  updateScore() {
    this.matchSummary.away = this.match.away.name;
    this.matchSummary.home = this.match.home.name;
    this.matchSummary.round = this.round;
    if (this.bracketService.validateMatch(this.matchSummary)) {
      this.updateMatch();
      this.bracketService.seasonResults.push(this.matchSummary);
      this.bracketService.matchSavedEmitter.next(this.matchSummary)
    } else {
      alert("Please fill all fields correctly");
    }

  }

}
