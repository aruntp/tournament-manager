import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { MatchStatComponent } from './match-stat.component';

describe('MatchStatComponent', () => {
  let component: MatchStatComponent;
  let fixture: ComponentFixture<MatchStatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchStatComponent ],
      providers: [{provide: ActivatedRoute, useValue: {
        params: of({id: 'Belgium'})
      }}, DatePipe, Router]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
