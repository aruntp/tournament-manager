import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatchInfo } from 'src/app/bracket/models/matchinfo';
import { TournamentService } from 'src/app/common-services/tournament.service';
import { MatchSummary } from '../models/matchsummary';
import { BracketService } from '../services/bracket.service';
import { first } from 'rxjs';
import { DatePipe } from '@angular/common';
import { BracketHttpService } from '../services/bracket-http.service';

@Component({
  selector: 'app-match-stat',
  templateUrl: './match-stat.component.html',
  styleUrls: ['./match-stat.component.scss']
})
export class MatchStatComponent implements OnInit {

  match: any;
  matchStats! : MatchInfo;
  homeScore: number = 0;
  awayScore: number = 0
  matchSummary: MatchSummary = new MatchSummary();

  constructor(private route: ActivatedRoute, 
    private datePipe: DatePipe,
    private http: BracketHttpService,
    private bracketService: BracketService,
    private tournamentService: TournamentService) { }

  /**
   * Get the match summary for detailed display
   * @param match - match input received from the brackets components
   * @returns - MatchSummary 
   */
  getMatchData(match: string): any {
    return this.bracketService.getMatchData(match);
  }

  ngOnInit(): void {
    const match = this.route.snapshot.params['id'];
    this.match = this.getMatchData(match);
  }

  /**
   * Not used now - Will be changed to this from card component
   */
  updateScore() {
    this.matchSummary.away = this.match.away.name;
    this.matchSummary.home = this.match.home.name;
    if (this.bracketService.validateMatch(this.matchSummary)) {
      this.tournamentService.updateQuarterFinalTeams(this.matchSummary);
      this.updateMatch();
      this.bracketService.matchSavedEmitter.next(this.matchSummary)
    }

  }

  /**
   * Not used now - Will be changed to this from card component
   */
  updateMatch() {
    if (this.matchSummary.id === '') {
      this.http.postMatch(this.matchSummary).pipe(first()).subscribe(
        (status: any) => {
          this.matchSummary.id = status.name;
        }
      )
    } else {
      this.http.updateMatch(this.matchSummary).pipe(first()).subscribe(
        (status: any) => {
          console.log(status);
        }
      )
    }
  }

  /**
   * Not used now - Will be changed to this from card component
   */
  saveDate(event: any) {
    this.matchSummary.date = this.datePipe.transform(event.value, 'yyyy-MM-dd');
  } 

}
