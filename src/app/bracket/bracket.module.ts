import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BracketHomeComponent } from './bracket-home/bracket-home.component';
import { BracketRoutingModule } from './bracket-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatchCardComponent } from './match-card/match-card.component';
import { MatchStatComponent } from './match-stat/match-stat.component';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    BracketHomeComponent,
    MatchCardComponent,
    MatchStatComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    BracketRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class BracketModule { }
