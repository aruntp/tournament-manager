import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BracketHomeComponent } from './bracket-home/bracket-home.component';
import { MatchStatComponent } from './match-stat/match-stat.component';

const bracketRoutes: Routes = [
    { path: 'brackets', component: BracketHomeComponent },
    { path: 'brackets/match/:id', component: MatchStatComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(bracketRoutes)],
  exports: [RouterModule]
})
export class BracketRoutingModule { }