import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first, Subscription } from 'rxjs';
import { Country } from 'src/app/models/country';
import { TournamentService } from 'src/app/common-services/tournament.service';
import { MatchSummary, ROUNDS } from '../models/matchsummary';
import { BracketHttpService } from '../services/bracket-http.service';
import { BracketService } from '../services/bracket.service';

@Component({
  selector: 'app-bracket-home',
  templateUrl: './bracket-home.component.html',
  styleUrls: ['./bracket-home.component.scss']
})
export class BracketHomeComponent implements OnInit, OnDestroy {

  round16Teams: Country[] = [];
  r16Fixtures: Country[] = [];
  quarterFinals: Country[] = [];
  semiFinals: Country[] = [];
  final: Country[] = [];
  finalReached = false;

  private matchSavedEventSubscription!: Subscription;

  constructor(private tournamentService: TournamentService,
    private bracketService: BracketService,
    private http: BracketHttpService,
    private router: Router) { }

    
  ngOnDestroy(): void {
    this.matchSavedEventSubscription.unsubscribe();
  }

  /**
   * Init component with R16 team creation
   * Subscribe to event so that each match saved from bracket is updated
   */
  ngOnInit(): void {
    this.r16Fixtures = this.bracketService.fixtures = this.bracketService.createBracket(ROUNDS.ROUND16);
    this.matchSavedEventSubscription = this.bracketService.matchSavedEmitter.subscribe((match: MatchSummary) => {
      this.updateMatch(match);
    });
  }

  /**
   * Save an entire season to database
   * TODO : Update in case currently existing season, not create another
   */
  saveSeason(): void {
    this.http.saveSeason(this.bracketService.seasonResults).pipe(first()).subscribe(
      status => {
        alert('Season saved successfully');
      }
    )
  }

  /**
   * Update the match results, and check if round bracket is covered
   * Update the next round participants based on the result and current round
   * @param match Match that is saved
   */
  updateMatch(match: MatchSummary): void {
    switch(match.round) {
      case ROUNDS.ROUND16 : 
        this.updateQuarterFinalists(match);
        break;
      case ROUNDS.QUARTERFINAL : 
        this.updateSemiFinalists(match);
        break;   
      case ROUNDS.SEMIFINAL :
        this.updateFinalists(match);
        break;
      default:
        break;                                                   
    }
  }

  /**
   * Update the teams for QF, from the R16 result
   * @param match - match details that is saved
   */
  updateQuarterFinalists(match: MatchSummary): void {
    const teamCount = this.tournamentService.updateQuarterFinalTeams(match);
    if (teamCount == 8) {
      this.quarterFinals = this.bracketService.createBracket(ROUNDS.QUARTERFINAL);
      this.bracketService.fixtures = this.bracketService.fixtures.concat(this.quarterFinals);
    }
  }

  /**
   * Update the teams for SF, from the QF result
   * @param match - match details that is saved
   */
  updateSemiFinalists(match: MatchSummary): void {
    const teamCount = this.tournamentService.updateSemiFinalTeams(match);
    if (teamCount == 4) {
      this.semiFinals = this.bracketService.createBracket(ROUNDS.SEMIFINAL);
      this.bracketService.fixtures = this.bracketService.fixtures.concat(this.semiFinals);
    }
  }

  /**
   * Update the teams for Final, from the SF result
   * @param match - match details that is saved
   */
  updateFinalists(match: MatchSummary): void {
    const teamCount = this.tournamentService.updateFinalTeams(match);
    if (teamCount == 2) {
      this.finalReached = true;
      this.final = this.bracketService.createBracket(ROUNDS.FINAL);
      this.bracketService.fixtures = this.bracketService.fixtures.concat(this.final);
    }
  }

  /**
   * 
   * @param index - the index of fixture id, in the complete bracket. 
   * @param round - pass the required round info, so that the index can be calculated
   */
  viewMatchDetails(index: number, round: number): void {
    this.router.navigate(['/brackets/match', index + '-' + round]);
  }

}
