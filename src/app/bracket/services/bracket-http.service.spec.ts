import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { BracketHttpService } from './bracket-http.service';

describe('BracketHttpService', () => {
  let service: BracketHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpClient, HttpHandler]
    });
    service = TestBed.inject(BracketHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
