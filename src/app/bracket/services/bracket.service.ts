import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Country } from 'src/app/models/country';
import { TournamentService } from 'src/app/common-services/tournament.service';
import { MatchSummary, ROUNDS } from '../models/matchsummary';

@Injectable({
  providedIn: 'root'
})
export class BracketService {

  /**
   * All dirty logics are here for now
   */

  savedR16Matches: any;
  matchSavedEmitter = new Subject<MatchSummary>();
  fixtures: Country[] = [];
  seasonResults: MatchSummary[] = [];
  round16Teams: Country[] = [];
  r16Fixtures: Country[] = [];
  quarterFinals: Country[] = [];
  semiFinals: Country[] = [];
  final: Country[] = [];
  finalReached = false;

  constructor(private tournamentService: TournamentService) { }

  /**
   * Generate the brackets for all knockout rounds
   * @param round - number indicating the round 
   * @returns - the created bracket with teams info
   */
  createBracket(round: number): Country[] {
    let shuffled: any = [];
    switch (round) {
      case ROUNDS.QUARTERFINAL:
        shuffled = this.tournamentService.quarterFinalists;
        break;
      case ROUNDS.SEMIFINAL:
        shuffled = this.tournamentService.semiFinalists;
        break;
      case ROUNDS.FINAL:
        shuffled = this.tournamentService.finalists;
        break;
      default:
        shuffled = this.tournamentService.selectedTeams.filter(a => a.rank <= 16);
        break;
    }

    const teamOrder = this.createSeedOrder(shuffled);
    if (round != ROUNDS.ROUND16) {
      return this.createLastRoundsFixtures(shuffled);
    }
    return this.createRound16Fixtures(shuffled, teamOrder);
  }

  /**
   * Creates the fixtures for QF, SF and Final
   * @param teams - The list of teams eligible for the round
   * @returns - the fixture list as object
   */
  createLastRoundsFixtures(teams: Country[]) {
    let matchObj: any[] = []
    for (let i = 0; i < teams.length; i += 2) {
      const home = teams[i];
      const away = teams[i + 1];
      matchObj.push({ home: home, away: away });
    }
    return matchObj;
  }

  /**
   * Creates the fixture of 8 matches for Round of 16
   * @param teams - teams eligible for R16 participation
   * @param teamOrder - Top 16 teams from the available teams automatically for the round 16
   * @returns - fixture created
   */
  createRound16Fixtures(teams: Country[], teamOrder: any[]) {
    let matchObj: any[] = []
    for (let i = 0; i < teamOrder.length; i += 2) {
      const home = teams.find(team => team.rank == teamOrder[i]);
      const away = teams.find(team => team.rank == teamOrder[i + 1]);
      matchObj.push({ home: home, away: away });
    }
    return matchObj;
  }

  /**
   * Sets the order of seeding in array, for the fixture creation
   * @param teams - The list of teams available for the round
   * @returns - the order of seeding
   */
  createSeedOrder(teams: any[]) {
    var rounds = Math.log(teams.length) / Math.log(2) - 1;
    var order = [1, 2];
    for (var i = 0; i < rounds; i++) {
      order = this.nextTwoTeams(order);
    }
    return order;
  }


  /**
 * Returns the match requested from by the user for viewing the details
 * @param match - data of the match which is to be returned with details
 * @returns 
 */
  getMatchData(match: string): any {
    const matchId = Number(match.split('-')[0]);
    const round = Number(match.split('-')[1]);

    if (matchId == -1) {
      return this.fixtures[this.fixtures.length - 1];
    } else if (round == 0) {
      return this.fixtures[matchId];
    } else if (round == 1) {
      return this.fixtures[matchId + 15];
    } else if (round == 2) {
      return this.fixtures[matchId + 23];
    }

    return this.fixtures[matchId + 25];

  }

  /**
   * 
   * @param order - place holder for the current team in the seed order
   * @returns - updated position
   */
   nextTwoTeams(order: any) {
    let out: any = [];
    var length = order.length * 2 + 1;
    order.forEach(function (d: any) {
      out.push(d);
      out.push(length - d);
    });
    return out;
  }

  /**
   * validate if all fields have been inserted 
   * @param matchSummary - the match data from the UI
   * @returns - true, if all fields are correct, else false
   */
  validateMatch(matchSummary: MatchSummary) {
    let status = true;
    if (matchSummary.awayGoals == matchSummary.homeGoals) {
      status = false;
    }
    if (matchSummary.date == null) {
      status = false
    }
    return status;
  }


}
