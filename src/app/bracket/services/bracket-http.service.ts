import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatchSummary } from '../models/matchsummary';

@Injectable({
  providedIn: 'root'
})
export class BracketHttpService {

  /**
   * All http calls used by the Bracket module resides here
   */
  apiUrl = "https://tournament-server-default-rtdb.europe-west1.firebasedatabase.app/";
  constructor(private http: HttpClient) { }

  postMatch(match: MatchSummary) {
    return this.http.post(this.apiUrl + 'matches.json', match);
  }

  updateMatch(match: MatchSummary) {
    return this.http.put(this.apiUrl + 'matches/' + match.id + '.json', match);
  }

  saveSeason(season: MatchSummary[]) {
    return this.http.post(this.apiUrl + 'seasons.json', season);
  }
  

}
