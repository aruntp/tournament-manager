/**
 * Summary which is showsn in the Brackets screen cards
 * Format used in the backend for storing data
 */
export class MatchSummary {
    id: string = '';
    round: number = 0;
    home: string = '';
    away: string= '';
    homeGoals: number = 0;
    awayGoals: number = 0
    date: any;
}

export enum ROUNDS {
    ROUND16 = 0,
    QUARTERFINAL,
    SEMIFINAL,
    FINAL
}