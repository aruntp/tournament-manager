/**
 * Model for displaying the data in the match ststs component
 */
export interface MatchInfo {
    matchDate : Date;
    shots: number[];
    shotsTarget: number[];
    possession: number[];
    passes: number[];
    passAccuracy: number[];
    fouls: number[];
    yellowCards: number[];
    redCards: number[];
}