import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { CountryresolverService } from './countryresolver.service';

describe('CountryresolverService', () => {
  let service: CountryresolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpClient, HttpHandler]
    });
    service = TestBed.inject(CountryresolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
