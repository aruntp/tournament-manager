import { TestBed } from '@angular/core/testing';
import { CachingInterceptor } from './cacheinterceptor.service';


describe('CacheinterceptorService', () => {
  let service: CachingInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CachingInterceptor]
    });
    service = TestBed.inject(CachingInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
