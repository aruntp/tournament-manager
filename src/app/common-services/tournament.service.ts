import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatchSummary } from '../bracket/models/matchsummary';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class TournamentService {

  generateEmit = new Subject<Country[]>();
  selectedTeams: Country[] = [];
  quarterFinalists: Country[] = [];
  semiFinalists: Country[] = [];
  finalists: Country[] = [];

  constructor() { }

  /**
   * get the teams selected for R16
   * @returns The selected teams list for R16
   */
  getSelectedTeams() : Country[] {
    return this.selectedTeams;
  }
  
  /**
   * Find the winner from the match result entered
   * @param matchSummary - match data from UI
   * @returns 
   */
  getWinner(matchSummary: MatchSummary) {
    let winner: string = '';
    if (matchSummary.awayGoals > matchSummary.homeGoals) {
      winner = matchSummary.away;
    } else {
      winner = matchSummary.home;
    }
    return this.selectedTeams.find(c => c.name == winner);
    
  }

  /**
  * Adds the team for final
  * @param matchSummary - match result of semi final
  * @returns - returns the teams length for the final
  */
  updateFinalTeams(matchSummary: MatchSummary) {
    let team : any;
    team = this.getWinner(matchSummary);
    if (!this.finalists.find(t => t == team))
      this.finalists.push(team);

    return this.finalists.length;
  }

  /**
   * Adds the team for QF based on the R16 result
   * @param matchSummary - match result of R16 game
   * @returns - length of the teams qualified for QF
   */
  updateQuarterFinalTeams(matchSummary: MatchSummary): number {
    let team : any;
    team = this.getWinner(matchSummary);
    if (!this.quarterFinalists.find(t => t == team))
      this.quarterFinalists.push(team);

    return this.quarterFinalists.length;
  }

  /**
   * Adds the team for SF based on the QF result
   * @param matchSummary - match result of QF game
   * @returns - length of the teams qualified for SF
   */
  updateSemiFinalTeams(matchSummary: MatchSummary) {
    let team : any;
    team = this.getWinner(matchSummary);
    if (!this.semiFinalists.find(t => t == team))
      this.semiFinalists.push(team);

    return this.semiFinalists.length;
  }

}
