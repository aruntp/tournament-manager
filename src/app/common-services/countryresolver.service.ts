import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { CountryDataService } from '../country/country-data.service';

@Injectable({
  providedIn: 'root'
})
export class CountryresolverService  implements Resolve<Observable<any>>{

  constructor(public countrySrv: CountryDataService) { }
   resolve() {
      return this.countrySrv.getCountries()
   }
}
