import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CountryModule } from './country/country.module';
import { BracketModule } from './bracket/bracket.module';
import { PlayerModule } from './player/player.module';
// import { CachingInterceptor } from './common-services/cacheinterceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    CountryModule,
    PlayerModule,
    BracketModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
//   providers: [ {
//       provide: HTTP_INTERCEPTORS,
//       useClass: CachingInterceptor,
//       multi: true
//    }
// ],
  bootstrap: [AppComponent]
})
export class AppModule { }
