import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs';
import { CountryDataService } from 'src/app/country/country-data.service';
import { Country } from 'src/app/models/country';
import { TournamentService } from 'src/app/common-services/tournament.service';

@Component({
  selector: 'app-player-home',
  templateUrl: './player-home.component.html',
  styleUrls: ['./player-home.component.scss']
})
export class PlayerHomeComponent implements OnInit {
  myControl = new FormControl();
  options: Country[] = []
  teamData: any = [];
  constructor(private route: ActivatedRoute,
     private countrySrv: CountryDataService,
     private tournamentService: TournamentService) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.params);
    this.options = this.tournamentService.selectedTeams;

    const countryName = this.route.snapshot.params['id'];
    if (countryName) {
      this.getCountryData(countryName);
      this.myControl.setValue(countryName);
    }
  }

  getCountryData(countryName: string) {
    this.countrySrv.getCountryDetails(countryName).pipe(first()).subscribe(
      data => {
        this.teamData = data;
      }
    )
  }

  viewCountry(event: any) {
    this.getCountryData(event.value);
  }

  viewPlayer(player: string) {

  }

}
