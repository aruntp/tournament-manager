import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerHomeComponent } from './player-home/player-home.component';
import { AddPlayerComponent } from './add-player/add-player.component';

import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { PlayerRoutingModule } from './player-routing.module';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    PlayerHomeComponent,
    AddPlayerComponent
  ],
  imports: [
    CommonModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    PlayerRoutingModule,
  ],
})
export class PlayerModule { }
