import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPlayerComponent } from './add-player/add-player.component';
import { PlayerHomeComponent } from './player-home/player-home.component';

const playerRoutes: Routes = [
    { path: 'players', component: PlayerHomeComponent },
    { path: 'players/player', component: AddPlayerComponent },
    { path: 'players/:id', component: PlayerHomeComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(playerRoutes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }